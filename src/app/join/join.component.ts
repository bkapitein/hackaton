import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

export interface Player {
  name: string;
  gender: string;
  teamNumber: number;
}

const ELEMENT_DATA: Player[] = [
  {name: 'Geerte van der Vooren', gender: 'F', teamNumber: 1},
  {name: 'Bo in \'t Veen', gender: 'F', teamNumber: 1},
  {name: 'Rafaël Rikken', gender: 'M', teamNumber: 1},
  {name: 'Lorraine Overwijk', gender: 'F', teamNumber: 1},
  {name: 'Romijn van Schoonhoven', gender: 'M', teamNumber: 2},
  {name: 'Seine van de Ven', gender: 'M', teamNumber: 2},
  {name: 'Lilianne Dijkslag', gender: 'F', teamNumber: 2},
  {name: 'Giuliano Bach', gender: 'M', teamNumber: 2},
];

@Component({
  selector: 'app-join',
  templateUrl: 'join.component.html',
  styles: [
    'table { width: 100%; }'
  ]
})
export class JoinComponent implements OnInit {
  displayedColumns: string[] = ['name', 'gender', 'teamNumber'];
  dataSource: Player[] = ELEMENT_DATA;
  code: string | null = '';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.code = this.route.snapshot.paramMap.get('code')
    this.generate();
  }

  generate(): void {
    this.dataSource.forEach(e => {
      e.teamNumber = this.getRandomInt(2);
    });
  }

  getRandomInt(max: number) {
    return Math.floor(Math.random() * max) + 1;
  }
}
