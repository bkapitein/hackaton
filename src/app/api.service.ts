import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { GameEvent } from './models';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly gameEvents$ = new BehaviorSubject<GameEvent[]>([]);
  constructor() { }

  public createNewGame(gameName: string, organisatorName: string): Observable<GameEvent | null>
  {
    const gameEvent: GameEvent = {
      id: uuid.v4(),
      name: gameName,
      joinCode: 'ABCDE', 
      participants: [
        {
          id: uuid.v4(),
          name: organisatorName,
        }
      ],
    };

    this.gameEvents$.next([...this.gameEvents$.value, gameEvent]);
    return of(gameEvent);
  }

  public joinGame(joinCode: string, participanName: string): Observable<GameEvent | null>
  { 
    const gameEvent = this.gameEvents$.value.find(ge => ge.joinCode === joinCode);
    if(gameEvent === undefined) {
      throw new Error('game not found');
    }

    const newGameEvent = {
      ...gameEvent,
      participants: [
        ...gameEvent.participants,
        { 
          id: uuid.v4(),
          name: participanName,
        }
      ]
    };

    this.gameEvents$.next(
      [
        ...this.gameEvents$.value.filter(ge => ge.id !== gameEvent.id),
        newGameEvent
      ]
    );


    return of(newGameEvent);
  }
}
