export type GameEvent = {
    id: string;
    name: string;
    joinCode: string;
    participants: Participant[];
}

export type Participant = {
    id: string;
    name: string;
}