import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { JoinComponent } from './join/join.component';
import { NewGameComponent } from './new-game/new-game.component';

const routes: Routes = [
  { path: 'new/:name', component: NewGameComponent },
  { path: 'join/:code', component: JoinComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
