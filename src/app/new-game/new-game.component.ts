import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-game',
  templateUrl: 'new-game.component.html',
})
export class NewGameComponent implements OnInit {
  name: string | null = '';
  constructor(private readonly route: ActivatedRoute) { }

  ngOnInit(): void {
    this.name = this.route.snapshot.paramMap.get('name');
  }

}
