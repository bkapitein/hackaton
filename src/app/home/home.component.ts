import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: [
    'home.component.css',
  ]
})
export class HomeComponent implements OnInit {

  constructor(private readonly router: Router) { }

  public gameName = '';
  public joinCode = '';

  ngOnInit(): void {
  }

  public onNewClick()
  {
    this.router.navigateByUrl('new/'+ this.gameName);
  }

  public onJoinClick()
  {
    this.router.navigateByUrl('join/'+ this.joinCode);
  }

}
